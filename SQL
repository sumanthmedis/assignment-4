import java.sql.SQLException;
public class SQLExceptionDemo {
    public static void main(String[] args) {
        try {
            // Create a new SQLException object with a message
            SQLException ex1 = new SQLException("This is an SQLException");
            System.out.println("ex1.getMessage(): " + ex1.getMessage());
            
            // Create a new SQLException object with a message and SQL state
            SQLException ex2 = new SQLException("This is another SQLException", "SQLState");
            System.out.println("ex2.getMessage(): " + ex2.getMessage());
            System.out.println("ex2.getSQLState(): " + ex2.getSQLState());
            
            // Create a new SQLException object with a message, SQL state, and error code
            SQLException ex3 = new SQLException("This is yet another SQLException", "SQLState", 12345);
            System.out.println("ex3.getMessage(): " + ex3.getMessage());
            System.out.println("ex3.getSQLState(): " + ex3.getSQLState());
            System.out.println("ex3.getErrorCode(): " + ex3.getErrorCode());
            
            // Create a new SQLException object with a message, SQL state, error code, and cause
            SQLException ex4 = new SQLException("This is the final SQLException", "SQLState", 67890, ex3);
            System.out.println("ex4.getMessage(): " + ex4.getMessage());
            System.out.println("ex4.getSQLState(): " + ex4.getSQLState());
            System.out.println("ex4.getErrorCode(): " + ex4.getErrorCode());
            System.out.println("ex4.getCause(): " + ex4.getCause());
            throw new SQLException("An error occurred.");
        } 
        catch(SQLException e) 
        {
            e.printStackTrace();
    }
        
    }
}
